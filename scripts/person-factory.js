const getImage = (image, title) => {
    switch (title) {
        case 'VIP':
            return `vips/${image}.png`
        case 'SUBSCRIBER':
            return `subs/${image}.png`
        case 'MOD':
            return `mods/${image}.png`
    }
}

const personFactory = (name, title) => ({
    name,
    title,
    image: getImage(name, title)
})
