const app = document.getElementById('app')

const SUBSCRIBER_PRIMARY_INDEX = 0;
const SUBSCRIBER_SECONDARY_INDEX = 1;
const MODS_INDEX = 2;
const VIPS_INDEX = 3;
const TIME_TO_CHANGE_IN_SECONDS = 5

const generateIndexes = () => ([
    parseInt(Math.random() * SUBSCRIBERS.length),
    parseInt(Math.random() * SUBSCRIBERS.length),
    parseInt(Math.random() * MODS.length),
    parseInt(Math.random() * VIPS.length)
])

// DEPRECATED
// const isUniquesIndexes = (listIndexes) => {
//     const filteredList = listIndexes.filter((elem, pos, arr) => {
//         return arr.indexOf(elem) == pos;
//     })

//     return filteredList.length === listIndexes.length
// }

const isUniqueSubscriberIndexes = (listIndexes) => {
    return listIndexes[SUBSCRIBER_PRIMARY_INDEX] !== listIndexes[SUBSCRIBER_SECONDARY_INDEX]
}

const resetHTML = () => app.innerHTML = '';

const makeHTML = (listPartiners) => {
    listPartiners.forEach(partiner => {
        app.innerHTML += partiner
    });
}

const allPartiners = () => {
    makeHTML(SUBSCRIBERS)
    makeHTML(MODS)
    makeHTML(VIPS)
}

const showList = () => {
    let indexes = generateIndexes()
    
    resetHTML()

    while(!isUniqueSubscriberIndexes(indexes)) {
        indexes = generateIndexes()
    }
    
    const partinersList = [
        SUBSCRIBERS[indexes[SUBSCRIBER_PRIMARY_INDEX]],
        SUBSCRIBERS[indexes[SUBSCRIBER_SECONDARY_INDEX]],
        MODS[indexes[MODS_INDEX]],
        VIPS[indexes[VIPS_INDEX]],
    ]

    makeHTML(partinersList)
    // allPartiners()
}

showList()
setInterval(() => {
    showList()
}, TIME_TO_CHANGE_IN_SECONDS * 1000)
