const getTitlePerson = (title) => {
    switch (title) {
        case 'VIP':
            return 'vip'
        case 'SUBSCRIBER':
            return 'subscriber'
        case 'MOD':
            return 'mod'
    }
}

const getDescription = (title) => {
    switch (title) {
        case 'VIP':
            return 'VIP Sinistro'
        case 'SUBSCRIBER':
            return 'Super Inscrito'
        case 'MOD':
            return 'MOD Brabo'
    }
}

const cardFactory = ({name, title, image}) => {
    return `
        <div class="person-container ${getTitlePerson(title)}">
            <img src="./images/${image}" alt="">
            <div class="person-infos">
                <h2>${name.substr(0, 10)}</h2>
                <p>
                    ${getDescription(title)}
                </p>
            </div>
            <img src="./images/${getTitlePerson(title)}.png" class="${getTitlePerson(title)}-icon" alt="">
        </div>
    `
}
